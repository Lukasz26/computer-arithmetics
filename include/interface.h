#ifndef _INTERFACE_H_
#define _INTERFACE_H_

#include <curses.h>
#include <stdbool.h>

typedef unsigned long bitword;

typedef void (*binaryWindowClickHandler)(int bit);
typedef void (*commandWindowClickHandler)();
typedef char* (*outputWindowGetTextHandler)();

typedef struct {
    WINDOW* win;
    char* title;
    short color;
    int bitsize;
    bitword* bits;
    bitword* selectedBits;
    binaryWindowClickHandler onClick;
} BINARYWINDOW;

typedef struct {
    WINDOW* win;
    char* title;
    short color;
    commandWindowClickHandler onClick;
} COMMANDWINDOW;

typedef struct {
    WINDOW* win;
    char* title;
    short color;
    outputWindowGetTextHandler onGetText;
} OUTPUTWINDOW;

bool initInterface();
void closeInterface();

void eventLoop();

bool newBinaryWin(BINARYWINDOW* winStruct, int y, int x,
                  char* title, short color, int bitsize,
                  bitword* bits, bitword* selectedBits,
                  binaryWindowClickHandler onClick);

bool newCommandWin(COMMANDWINDOW* winStruct, int y, int x, int width,
                  char* title, short color,
                  commandWindowClickHandler onClick);

bool newOutputWin(OUTPUTWINDOW* winStruct, int y, int x, int width,
                  char* title, short color,
                  outputWindowGetTextHandler onGetText);

enum {
    BIT_WIDTH = 3,
    BIT_HEIGHT = 3,
    COMMAND_HEIGHT = 3,
    OUTPUT_HEIGHT = 4
};

#endif